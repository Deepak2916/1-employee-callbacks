/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/


const { log } = require('console')
const fs = require('fs')
const path = require('path')

function retrieveDataForIds(err, data) {
    if (err) {
        console.log(err)
    } else {
        const jsonData = JSON.parse(data).employees
        const result = jsonData.filter(employee => {
            return [2, 13, 23].includes(employee.id)
        })
        const filePath = path.join(__dirname, 'retrieveDataForIds.json')
        writeFiles(filePath, result, jsonData, groupDataBasedOnCompanies)
    }
}

function groupDataBasedOnCompanies(err, data) {
    if (err) {
        console.log(err)
    }
    else {

        const result = data.reduce((groupedData, employee) => {

            groupedData[employee.company].push(employee)

            return groupedData
        }, {
            "Scooby Doo": [],
            "Powerpuff Brigade": [],
            "X-Men": []
        })
        // console.log(result);
        const filePath = path.join(__dirname, 'groupDataBasedOnCompanies.json')
        writeFiles(filePath, result, data, dataForCompanyPowerpuffBrigade)
    }
}

function dataForCompanyPowerpuffBrigade(err, data) {
    if (err) {

    } else {

        const result = data.filter(employee => {
            return employee.company === "Powerpuff Brigade"
        })
        const filePath = path.join(__dirname, 'dataForCompanyPowerpuffBrigade.json')
        writeFiles(filePath, result, data, removeEntryWithId2)
    }

}

function removeEntryWithId2(err, data) {
    if (err) {
        console.log(err)
    } else {

        const result = data.filter(employee => {
            return employee.id !== 2
        })
        const filePath = path.join(__dirname, 'removeEntryWithId2.json')
        writeFiles(filePath, result, data, sortDataBasedOnCompanyName)
    }
}

function sortDataBasedOnCompanyName(err, data) {
    if (err) {
        console.log(err)
    } else {

        const result = data.sort((data1, data2) => {
            return data1.id - data2.id
        })
            .sort((data1, data2) => {
                const companyName1 = data1.company.toLowerCase()
                const companyName2 = data2.company.toLowerCase()
                if(companyName2 > companyName1){
                    return -1
                }else{
                    return 1
                }
            })
        const filePath = path.join(__dirname, 'sortDataBasedOnCompanyName.json')
        writeFiles(filePath, data, result, swapPositionOfCompanies)
    }
}

function swapPositionOfCompanies(err, data) {
    //companies with id 93 and id 92
    if (err) {
        console.log(err)
    } else {

        const result = [...data]
        const companyWithId = data.filter((employee) => {
            return employee.id === 92 || employee.id === 93
        })
        const indexOfId93 = data.indexOf(companyWithId[0])
        const indexOfId92 = data.indexOf(companyWithId[1])

        data[indexOfId92] = companyWithId[0]
        data[indexOfId93] = companyWithId[1]

        const filePath = path.join(__dirname, 'swapPositionOfCompanies.json')
        writeFiles(filePath, data, result, addTheBirthdayToEmployeeWhoseIdIsEven)
    }


}

function addTheBirthdayToEmployeeWhoseIdIsEven(err, data) {
    if (err) {
        console.log(err)
    } else {

        let date = new Date()
        const result = data.filter(employee => {
            return employee.id % 2 === 0
        })
            .map(employee => {
                employee['Birthday'] = date
                return employee
            })
        const filePath = path.join(__dirname, 'addTheBirthdayToEmployeeWhoseIdIsEven.json')
        writeFiles(filePath, data, result, (err, data) => {
            if (err) {
                console.log(err)
            } else {
                console.log('completed');
            }
        })
    }
}


const dataPath = path.join(__dirname, 'data.json')


fs.readFile(dataPath, 'utf-8', retrieveDataForIds)

function writeFiles(filePath, data, jsonData, callback) {
    const stringfyData = JSON.stringify(data, null, 2)

    fs.writeFile(filePath, stringfyData, err => {
        if (err) {
            callback(err)
        } else {
            callback(null, jsonData)
        }
    })
}